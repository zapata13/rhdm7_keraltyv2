[when]Cuando haya un paciente=paciente : Paciente()
[when]- que sea mayor {edad} años con examen de  glucosa en suero en {glucosaEnSuero}=(paciente.getEdad() > {edad} && paciente.getGlucosaEnSuero() == {glucosaEnSuero}) 
[when]- que tenga indice de masa corporal mayor a {imc}=(paciente.getImc() > {imc}) 
[when]- que tenga genero {genero} ademas de perimetro abdominal mayor a {perimetroAbdominal}=(genero == "{genero}" && perimetroAbdominal > {perimetroAbdominal}) 
[when]- que tenga herencia de diabetes grado uno {herenciaDiabetesGradoUno} o grado dos {herenciaDiabetesGradoDos}=(herenciaDiabetesGradoUno == {herenciaDiabetesGradoUno} || herenciaDiabetesGradoDos == {herenciaDiabetesGradoDos}) 
[when]- que tenga enfermedades cardiovasculares {codCardiovascular}=(codCardiovascular == "{codCardiovascular}") 
[when]- que tenga sistole mayor a {hipertensionSistolico} diastole mayor a {hipertensionDiastolico} medicamentos de hipertension en {medicamentosHipertension}=(hipertensionSistolico > {hipertensionSistolico} || hipertensionDiastolico > {hipertensionDiastolico} || medicamentosHipertension == {medicamentosHipertension}) 
[when]- que tenga colesterol menor a {hdl} trigliseridos mayor a {trigliceridos}=(hdl < {hdl} || trigliceridos > {trigliceridos}) 
[when]- que tenga genero {genero} y ovario poliquistico en {ovarioPoliquistico}=(genero == "{genero}" && ovarioPoliquistico == "{ovarioPoliquistico}") 
[when]- que tenga sedentarismo igual a {sedentarismo}=(sedentarismo == {sedentarismo}) 
[when]- que tenga herencia de diabetes gestacional sea igual a {antecedentesDiabetesGestacional}=(antecedentesDiabetesGestacional == {antecedentesDiabetesGestacional}) 
[when]- que tenga hijos con mas de 4k al nacer {hijosMasCuatroKgAlNacer}=(hijosMasCuatroKgAlNacer == {hijosMasCuatroKgAlNacer}) 
[when]- que tenga acantosis nigricans igual a {acantosis}=(acantosis == "{acantosis}") 
[when]- que tenga puntaje findrisk mayor o igual a {findrisk}=(findrisk >= {findrisk})
[then]El paciente debe {mensaje}=modify( paciente ) \{ setMensaje("{mensaje}") \}
[then]Log codCardiovascular=System.out.println("el primer codigo es " + paciente.codCardiovascular[1]);
[then]Log rule name=System.out.println("Rule fired : [" + drools.getRule().getName()+"]");
